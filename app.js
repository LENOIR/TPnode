"use strict"

var express          = require('express')
var session          = require('cookie-session')
var bodyParser       = require('body-parser')
var mongoose         = require("mongoose")
var jade             = require('jade')
var colors           = require('colors')
const util           = require('util')
var jsonexport       = require('jsonexport')
var urlencodedParser = bodyParser.urlencoded({ extended: false })
var app              = express()
var port             = 8000
let db               = mongoose.connect("mongodb://localhost/mydb")
//var taches           = []
//var csv              = require("fast-csv")
//var fs               = require('fs');

mongoose.connection.on("error", function() {
	console.log("erreur mongoose lol noob".bgRed)
})
mongoose.connection.on("open", function() {
	console.log("Mongoose OK".bgGreen)
})
let tacheSchema = mongoose.Schema({
	nom:String,
	couleur:String,
	dateDebut:String,
	dateFin:String,
	parent:String,
	importance:String
})

let Tache=mongoose.model("Tache", tacheSchema)

app.use(express.static( __dirname +'/public'))

app.use(session({secret:"oui salut"}))

	.get('/', function(req, res) {
		Tache.find( function (err,taches){
			if (err) return console.error(err.bgRed)
				res.render('index.ejs',{todolist:taches});30})
	})

	.get('/api/list', function(req, res) {
		Tache.find( function (err,taches){
			if (err) return console.error(err.bgRed)
				res.render('list_only.ejs',{todolist:taches});30})
	})

	.post('/ajouter', urlencodedParser, function(req, res) {
		if (req.body.newtodo != '') {
			var currentTime = new Date()
			var day = currentTime.getDate()
			if (day < 10) { var day = "0" + day }
			var month = currentTime.getMonth() + 1
			if (month < 10) { var month = "0" + month }
			var year = currentTime.getFullYear()
			var date_str = day + "/" + month + "/" + year

			console.log((req.body.datefin+"").bgRed)

			var tache = new Tache({nom: req.body.newtodo, couleur: req.body.couleur.slice(1), dateDebut: date_str, dateFin: req.body.datefin, parent: "", importance: req.body.importance})
			tache.save(function (err, tache) { if (err) return console.error(err.bgRed) })
		}
		res.redirect('/')
	})

	.get('/supprimer/:id', function(req, res) {
		if (req.params.id != '') {
			Tache.remove({_id: req.params.id}, function(err) {if (err) {console.log( ('erreur suppression '+req.params.id).bgRed )} else { console.log( ('suppression reussie '+req.params.id).bgGreen ) }})
		}
		res.redirect('/')
	})

	.get('/edit/:id/:new/:couleur/:datefin/:importance/:datedebut', function(req, res) {
		if (req.params.new != '' && req.params.id != '') {
			var tmp = req.params.datefin
			var tmp2 = req.params.datedebut
			var tache = new Tache({
			  _id: req.params.id,
			  nom: req.params.new,
			  couleur: req.params.couleur,
			  dateFin: tmp.charAt(0)+tmp.charAt(1)+"/"+tmp.charAt(2)+tmp.charAt(3)+"/"+tmp.slice(4),
			  dateDebut: tmp2.charAt(0)+tmp2.charAt(1)+"/"+tmp2.charAt(2)+tmp2.charAt(3)+"/"+tmp2.slice(4),
			  importance: req.params.importance
			})

			Tache.remove({_id : req.params.id}, function (err) {
				if (err) console.log(('error while updating '+req.params.id).bgRed)
			})

			tache.save(function (err, tache) { if (err) return console.error(err.bgRed) })
		}
		res.redirect('/')
	})

	.get('/export', function(req, res) {
		
		Tache.find({}, function(err, taches) {
			if (!err){ 
				res.send(JSON.stringify(taches))
				// var ws = fs.createWriteStream("export.csv");
				// csv
				// 	.write(taches)
				// 	.pipe(ws)
				// console.log(taches)
				// res.sendFile(__dirname + "/export.csv")
			} else { throw err }
		})
	})

	.use(function(req, res, next) {
		console.log(("[404] " + req.url).bgRed)
		res.redirect('/')
	})

	.listen(port, function() {
		console.log("web OK".bgGreen)
	})